package com.udacity.gce.jokesapp;


public interface DelegateResponse {
    void processFinish(String output);
}
