package com.udacity.gce.jokesapp;

import android.support.test.runner.AndroidJUnit4;

import com.udacity.gce.jokeslibrary.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class EndpointsAsyncTaskTest {

    private EndpointsAsyncTask mAsyncTask;

    @Before
    public void createAsyncTask(){

        mAsyncTask = new EndpointsAsyncTask();
    }

    @Test
    public void testDoInBackground() throws Exception {
        String joke = mAsyncTask.execute().get();
        assertTrue("get joke failed",joke != null);
    }
}