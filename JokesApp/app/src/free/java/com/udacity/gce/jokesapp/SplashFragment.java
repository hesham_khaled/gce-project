package com.udacity.gce.jokesapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 */
public class SplashFragment extends Fragment implements DelegateResponse {

    private TimerTask splashTimerTask;
    private Timer splashTimer;

    public SplashFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_splash, container, false);
        EndpointsAsyncTask endpointsAsyncTask = new EndpointsAsyncTask();
        endpointsAsyncTask.delegateResponse = SplashFragment.this;
        endpointsAsyncTask.execute();
        return view;
    }


    @Override
    public void processFinish(final String output) {
        splashTimerTask = new TimerTask() {

            @Override
            public void run() {
                Intent main = new Intent("com.udacity.gce.jokeslibrary.JokesActivity");
                main.putExtra("PAID_VERSION", false);
                main.putExtra("JOKE", output);
                startActivity(main);
                getActivity().finish();
            }
        };
        splashTimer = new Timer();
        splashTimer.schedule(splashTimerTask, 3000);
    }
}
