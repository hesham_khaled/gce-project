package com.udacity.gce.jokeslibrary;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;


/**
 * A simple {@link Fragment} subclass.
 */
public class JokesFragment extends Fragment {

    private TextView textView;

    public JokesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jokes, container, false);
        boolean isPaidVersion = getArguments().getBoolean("PAID_VERSION");
        if (!isPaidVersion)
            loadAd((RelativeLayout) view);
        Button requestBtn = (Button) view.findViewById(R.id.request_joke);
        textView = (TextView) view.findViewById(R.id.joke_text);
        requestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String joke = getArguments().getString("JOKE");
                if (joke != null) {
                    textView.setText(joke);
                }
            }
        });
        return view;
    }


    protected void loadAd(RelativeLayout adContainer) {

        AdRequest adRequest = new AdRequest.Builder().build();

        AdView mAdView = new AdView(getActivity());
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId("ca-app-pub-5084160762510449/7959753412");
        adContainer.addView(mAdView);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.d("Hesham", "ad loaded");
            }
        });
        mAdView.loadAd(adRequest);
    }
}
