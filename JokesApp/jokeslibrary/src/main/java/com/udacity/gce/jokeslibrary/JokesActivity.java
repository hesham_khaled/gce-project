package com.udacity.gce.jokeslibrary;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class JokesActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jokes);
        boolean isPaidVersion = getIntent().getBooleanExtra("PAID_VERSION",true);
        String joke = getIntent().getStringExtra("JOKE");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        JokesFragment fragment = new JokesFragment();
        Bundle bundle = new Bundle();
        bundle.putString("JOKE", joke);
        bundle.putBoolean("PAID_VERSION",isPaidVersion);
        fragment.setArguments(bundle);
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }


}
